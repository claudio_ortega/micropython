import network
import umqtt.simple
import time
import _thread
import machine


def subscription_cb(topic, msg):
    print("received msg: topic:[{0}], msg:[{1}]".format(topic, msg))


def task_tx(client, topic_write, led_pin):
    loop = 0
    while True:
        loop = loop + 1
        led_pin.on()
        m = "msg#{0}".format(loop)
        client.publish(topic=topic_write, msg=m)
        led_pin.off()
        time.sleep(1)
 

def task_rx(client):
    while True:
        client.wait_msg()


def init_network( topic ) -> umqtt.simple.MQTTClient :

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect("ortitos", "noelia2034")

    while not wlan.isconnected():
        print('retrying the wifi initial connection')
        time.sleep(0.5)

    print("connected to wifi, network config:{0}".format(wlan.ifconfig()))
    print(wlan.config('essid'))
    print(wlan.status('rssi'))

    time.sleep(1)

    client = umqtt.simple.MQTTClient("esp32-cueva", "45.79.84.187", 1883) # linode.1.poplarlabs.net -- 45.79.84.187
    client.set_callback(subscription_cb)
    client.connect(True)
    client.subscribe(topic)

    return client


def test_simple_mqtt():    

    # connect an LED in series with a 470 ohm resistor to pin 26 and GND
    p26 = machine.Pin(26, machine.Pin.OUT)
    
    topic = "/poplar/2"
    mqtt_client = init_network(topic)

    _thread.start_new_thread(task_tx, (mqtt_client, topic, p26))
    _thread.start_new_thread(task_rx, (mqtt_client,))

    while True:
        time.sleep(10)

if __name__ == '__main__':
    test_simple_mqtt()


