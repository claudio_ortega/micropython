import machine
import time
import _thread
import network
import umqtt.simple


def task_mqtt_rx_pump(client):
    while True:
        client.wait_msg()


def task_uart_rx(uart, client, topic, activity_pin) :
    loop = 0
    while True:
        loop = loop + 1
        read_bytes = uart.read(1000)
        if read_bytes != None:
            activity_pin.on()
            client.publish(topic=topic, msg=read_bytes)
            activity_pin.off()
            print("loop={}".format(loop))


def new_wifi(ap_name:str, password:str):

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(ap_name, password)

    while not wlan.isconnected():
        print("waiting for the wifi connection to start")
        time.sleep(2.0)

    print("connected to wifi, network config:{}".format(wlan.ifconfig()))
    print("mac:{}".format(wlan.config('mac')))
    print("essid:{}".format(wlan.config('essid')))
    print("rssi:{}".format(wlan.status('rssi')))


def main():

    new_wifi("ortitos", "noelia2034")
    time.sleep(1)

    mqtt_client = umqtt.simple.MQTTClient("esp32", "45.79.84.187", 1883)  # linode.1.poplarlabs.net
    mqtt_client.connect(True)

    # leave GPIO_17 (TX2) open, and connect GPIO_16 (RX2) with the output from the stm32 board
    uart = machine.UART(1, baudrate=921600, tx=17, rx=16) 

    # use the built-in blue led to show activity 
    activity_pin = machine.Pin(2, machine.Pin.OUT)

    for i in range (1,3):
        activity_pin.on()
        time.sleep(0.5)
        activity_pin.off()
        time.sleep(0.5)

    _thread.start_new_thread(task_uart_rx, (uart, mqtt_client, "/poplar/2", activity_pin,))
    _thread.start_new_thread(task_mqtt_rx_pump, (mqtt_client,))

    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()


