import machine
import time
import network
import socket
import _thread


def new_wifi_client(ap_name: str, password: str):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(ap_name, password)
    loop = 0
    while not wlan.isconnected():
        loop = loop + 1
        print("waiting for the wifi connection to start, try #{}".format(loop))
        time.sleep(2.0)
    print("connected to wifi, essid:{}, rssi:{}, network config:{}".format(
        wlan.config('essid'),
        wlan.status('rssi'),
        wlan.ifconfig()))


def task_rx():
    # leave GPIO_17 (TX2) open, and connect GPIO_16 (RX2) with the output from the stm32 board
    uart = machine.UART(1, baudrate=921600, tx=17, rx=16)

    # internal GPIO_02 connected into the board's blue led
    activity_pin = machine.Pin(2, machine.Pin.OUT)

    # network config
    new_wifi_client("ortitos", "noelia2034")
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # where are we sending data to
    server_address = ("192.168.1.98", 20001)

    loop = 0
    while True:
        read_bytes = uart.read(1000)
        if read_bytes is not None:
            loop = loop + 1
            if loop == 20:
                loop = 0
            if loop == 0:
                activity_pin.on()
            udp_socket.sendto(read_bytes, server_address)
            if loop == 0:
                activity_pin.off()


def task_sentinel():
    # external GPIO_26
    p26 = machine.Pin(26, machine.Pin.OUT)
    while True:
        p26.value(not p26.value())
        time.sleep(0.25)


def main():
    _thread.start_new_thread(task_sentinel, ())
    _thread.start_new_thread(task_rx, ())

    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
