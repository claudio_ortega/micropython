import machine
import time


def rtc(set_time:bool):
    rtc = machine.RTC()
    if set_time:
        rtc.init((2021, 5, 22,  0, 16, 1,  30, 0)) 
    tuple = rtc.datetime() 
    print ( tuple )


if __name__ == '__main__':
    rtc(False)
