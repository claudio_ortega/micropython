import machine
import time
import _thread

def task_tx(uart):
    loop = 0
    while True:
        loop = loop + 1 
        msg = "loop:{}".format(loop)
        uart.write(msg)

def task_rx(uart):
    while True:
        read_bytes = uart.read(1024)
        if read_bytes != None:
            print("read_bytes:[{}]\n".format(read_bytes) )

if __name__ == '__main__':
    print("start - uart.py")

    uart = machine.UART(1, baudrate=921600, tx=17, rx=16) # to test, short pins GPIO16 and GPIO17

    _thread.start_new_thread(task_rx, (uart,))

    time.sleep(1) 
    _thread.start_new_thread(task_tx, (uart,))

    # need to keep the main thread alive
    while True:
        time.sleep(10)

