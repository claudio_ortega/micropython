import machine
import time
import math
import _thread

def task_adc(adc, _ignore):
    while True:
        val = adc.read_u16()  
        print("val:{}".format(val) )
        time.sleep(0.1)


if __name__ == '__main__':

    # connect pin 25 (DAC output) with pin 36 (ADC input)
    dac = machine.DAC(machine.Pin(25))
    adc = machine.ADC(machine.Pin(36)) 

    _thread.start_new_thread(task_adc, (adc, ""))

    while True:
        dac.write(0)
        time.sleep(1)
        dac.write(255)
        time.sleep(1)        