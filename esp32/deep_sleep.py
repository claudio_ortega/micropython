import machine
import time


def toggle(max):
    p2 = machine.Pin(2, machine.Pin.OUT)
    p26 = machine.Pin(26, machine.Pin.OUT)
    for loop in range(max):
        p2.value(not p2.value())
        p26.value(not p26.value())
        time.sleep(0.1)


def main_deep_sleep():

    use_deep_sleep = False

    if use_deep_sleep:
        print("start toggle (1)")
        toggle(50)
        print("end toggle (1)")
        machine.deepsleep(5000)
    else:
        while True:
            print("start toggle (2)")
            toggle(50)
            print("end toggle (2)")
            time.sleep(1)


if __name__ == '__main__':

    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('coming back from deep sleep')
    else:
        print('normal start')

    main_deep_sleep()
