import network
import umqtt.simple
import time
import _thread
import machine
import onewire
import ds18x20


def subscription_cb(topic, msg):
    print("topic:{0}, msg:{1}".format(topic, msg))


def task_tx(client, topic_write, ds_sensors, sensors, led_pin):
    loop = 0
    while True:
        loop = loop + 1
        ds_sensors.convert_temp()
        led_pin.off()
        time.sleep(1.0)
        led_pin.on()
        for sensor in sensors:
            msg = "sending loop:{0}, value:{1}".format(loop, ds_sensors.read_temp(sensor))
            client.publish(topic=topic_write, msg=msg)


def task_rx(client, _ignore):
    while True:
        client.check_msg()
        time.sleep(0.1)


def init_network( topic ) -> umqtt.simple.MQTTClient :

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect("ortitos", "noelia2034")

    while not wlan.isconnected():
        print('retrying the wifi initial connection')
        time.sleep(1)

    print("connected to wifi\n")
    print('network config:', wlan.ifconfig())
    time.sleep(1)

    client = umqtt.simple.MQTTClient("esp32", "linode.1.poplarlabs.net", 1883)
    client.set_callback(subscription_cb)
    client.connect(True)
    client.subscribe(topic)

    return client


def init_DS18X20():

    # connect a 4k7 resistor in between pin 22 and 3.3v
    ds_pin = machine.Pin(22)
    ds_sensors = ds18x20.DS18X20(onewire.OneWire(ds_pin))
    sensors = ds_sensors.scan()
    print('found devices: ', sensors)
    return ds_sensors, sensors


if __name__ == '__main__':

    topic = "/poplar/2"
    mqtt_client = init_network(topic)

    ds_sensors, sensors = init_DS18X20()
    if len(sensors) != 1:
        raise Exception("it should be one and only one sensor available")

    # connect an LED in series with a 470 ohm resistor to pin 26 and GND
    p26 = machine.Pin(26, machine.Pin.OUT)

    _thread.start_new_thread(task_tx, (mqtt_client, topic, ds_sensors, sensors, p26))
    _thread.start_new_thread(task_rx, (mqtt_client, ""))

    while True:
        time.sleep(10)


