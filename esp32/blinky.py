import machine
import time


def toggle(max):
    print("start - blinky.py")

    p2 = machine.Pin(2, machine.Pin.OUT)      # GPIO 2 && built in pin
    p26 = machine.Pin(26, machine.Pin.OUT)    # GPIO 26

    for loop in range(max):
        p2.value(not p2.value())
        p26.value(not p26.value())
        time.sleep(0.050)


if __name__ == '__main__':
    toggle(20)
