import machine
import time
import math

if __name__ == '__main__':

    # create a buffer containing a sine-wave
    buf = bytearray(128)
    for i in range(len(buf)):
        buf[i] = 128 + int(127 * math.sin(2 * math.pi * i / len(buf)))

    # connect pin 25 (DAC output) with pin 36 (ADC input)
    dac = machine.DAC(machine.Pin(25))

    while True:
        for i in range(len(buf)):
            dac.write( buf[i])
            time.sleep(0.01)
