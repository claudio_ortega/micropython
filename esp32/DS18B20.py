import machine
import onewire
import ds18x20
import time


def read_temperature():
    # connect a 4k7 resistor in between pin 22 and 3.3v
    ds_pin = machine.Pin(22)  
    ds_sensors = ds18x20.DS18X20(onewire.OneWire(ds_pin))
    sensors = ds_sensors.scan()
    print('found devices: ', sensors)

    if len(sensors) > 0:
        while True:
            ds_sensors.convert_temp()
            time.sleep(1.0)
            for sensor in sensors:
                print(ds_sensors.read_temp(sensor))
                print(sensor)
                #print("ROM = {} - Family = 0x{:02x}".format([hex(i) for i in sensor], sensor.family_code))

    print ("no sensors")


if __name__ == '__main__':
    read_temperature()


