import machine
import time

if __name__ == '__main__':
    print("start")
    pwm = machine.PWM(machine.Pin(27), freq=20000, duty=512)  
    time.sleep(3600)  # 1 hour
    pwm.deinit()
    print("stop")
